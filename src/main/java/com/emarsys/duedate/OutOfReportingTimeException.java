package com.emarsys.duedate;

/**
 * Thrown to indicate an invalid submit date.
 */
public class OutOfReportingTimeException extends RuntimeException {

    private static final String INVALID_SUBMIT_DATE = "Invalid submit date.";

    public OutOfReportingTimeException() {
        super(INVALID_SUBMIT_DATE);
    }
}
