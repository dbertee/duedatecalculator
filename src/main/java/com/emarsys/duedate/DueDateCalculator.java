package com.emarsys.duedate;

import java.time.LocalDateTime;

/**
 * Calculates due date for a given submit date and turnaround time.
 */
public interface DueDateCalculator {

    /**
     * Calculates due date by adding turnaround time to the given submit date.
     *
     * @param submitDate The date that turnaround time should be calculated from.
     *                   Should contain Year, Month, Day, Hours, Minutes.
     *                   Must not be null.
     * @param turnaroundTime The number of hours to be added to the date. Must be a positive integer.
     * @return The calculated date.
     */
    LocalDateTime calculateDueDate(LocalDateTime submitDate, int turnaroundTime);
}
