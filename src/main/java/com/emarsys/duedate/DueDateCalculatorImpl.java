package com.emarsys.duedate;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

/**
 * Calculates due date taking only workdays and working hours into account.
 */
public class DueDateCalculatorImpl implements DueDateCalculator {

    private static final int START_OF_WORKING_HOURS = 9;
    private static final int END_OF_WORKING_HOURS = 16;

    @Override
    public LocalDateTime calculateDueDate(final LocalDateTime submitDate, final int turnaroundTime) {
        LocalDateTime dueDate = submitDate;
        if (submitDate == null || !isTurnaroundTimeValid(turnaroundTime) || !isWorkDay(submitDate) || !isWorkingHour(submitDate)) {
            throw new OutOfReportingTimeException();
        } else {
            dueDate = addHours(dueDate, turnaroundTime);
        }
        return dueDate;
    }

    private boolean isTurnaroundTimeValid(int turnaroundTime){
        return turnaroundTime > 0;
    }
    private boolean isWorkDay(LocalDateTime submitDate) {
        return submitDate.getDayOfWeek() != DayOfWeek.SATURDAY
                && submitDate.getDayOfWeek() != DayOfWeek.SUNDAY;
    }

    private boolean isWorkingHour(LocalDateTime submitDate) {
        return submitDate.getHour() >= START_OF_WORKING_HOURS
                && submitDate.getHour() <= END_OF_WORKING_HOURS;
    }

    private LocalDateTime addHours(LocalDateTime submitDate, int turnaroundTime) {
        int hour = submitDate.getHour();
        while (turnaroundTime > 0) {
            if (hour >= END_OF_WORKING_HOURS) {
                hour = START_OF_WORKING_HOURS;
                submitDate = increaseWorkDay(submitDate);
            } else {
                hour++;
            }
            turnaroundTime--;
        }
        return submitDate.withHour(hour);
    }

    private LocalDateTime increaseWorkDay(LocalDateTime localDateTime) {
        if (localDateTime.getDayOfWeek() == DayOfWeek.FRIDAY) {
            localDateTime = localDateTime.plusDays(3);
        } else {
            localDateTime = localDateTime.plusDays(1);
        }
        return localDateTime;
    }
}
