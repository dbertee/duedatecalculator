package com.emarsys.duedate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit test for @{@link DueDateCalculatorImpl}.
 */
public class DueDateCalculatorImplTest {

    private static final int FIVE_HOURS = 5;
    private static final int EIGHT_HOURS = 8;
    private static final int FORTY_HOURS = 40;
    private static final String INVALID_SUBMIT_DATE = "Invalid submit date.";
    private static final LocalDate DATE_2018_07_02_MONDAY = LocalDate.of(2018, 7, 2);
    private static final LocalDate DATE_2018_07_03_TUESDAY = LocalDate.of(2018, 7, 3);
    private static final LocalDate DATE_2018_07_06_FRIDAY = LocalDate.of(2018, 7, 6);
    private static final LocalDate DATE_2018_07_09_MONDAY = LocalDate.of(2018, 7, 9);
    private static final LocalTime TIME_9_12 = LocalTime.of(9, 12);
    private static final LocalTime TIME_14_12 = LocalTime.of(14, 12);
    private static final LocalTime INVALID_TIME = LocalTime.of(17, 12);

    private DueDateCalculator underTest;

    @BeforeEach
    public void test() {
        underTest = new DueDateCalculatorImpl();
    }

    @Test
    public void calculateDueDateShouldReturnSameDayWithCorrectHoursWhenTurnaroundTimeIsWithinWorkingHours() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_02_MONDAY, TIME_9_12);
        int turnaroundTime = FIVE_HOURS;
        //WHEN
        LocalDateTime actual = underTest.calculateDueDate(localDateTime, turnaroundTime);
        //THEN
        LocalDateTime expected = LocalDateTime.of(DATE_2018_07_02_MONDAY, TIME_14_12);
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDueDateShouldReturnNextWorkingDayWithSameTimeWhenTurnaroundTimeIsEight() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_02_MONDAY, TIME_14_12);
        int turnaroundTime = EIGHT_HOURS;
        //WHEN
        LocalDateTime actual = underTest.calculateDueDate(localDateTime, turnaroundTime);
        //THEN
        LocalDateTime expected = LocalDateTime.of(DATE_2018_07_03_TUESDAY, TIME_14_12);
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDueDateShouldReturnNextMondayWhenSubmitTimeIsFridayAndTurnaroundTimeIsEight() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_06_FRIDAY, TIME_14_12);
        int turnaroundTime = EIGHT_HOURS;
        //WHEN
        LocalDateTime actual = underTest.calculateDueDate(localDateTime, turnaroundTime);
        //THEN
        LocalDateTime expected = LocalDateTime.of(DATE_2018_07_09_MONDAY, TIME_14_12);
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDueDateShouldReturnNextMondayWhenSubmitTimeIsMondayAndTurnaroundTimeIsForty() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_02_MONDAY, TIME_14_12);
        int turnaroundTime = FORTY_HOURS;
        //WHEN
        LocalDateTime actual = underTest.calculateDueDate(localDateTime, turnaroundTime);
        //THEN
        LocalDateTime expected = LocalDateTime.of(DATE_2018_07_09_MONDAY, TIME_14_12);
        assertEquals(expected, actual);
    }

    @Test
    public void calculateDueDateShouldThrowOutOfReportingTimeExceptionWhenSubmitDateIsOutOfOfficeHours() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_02_MONDAY, INVALID_TIME);
        int turnaroundTime = EIGHT_HOURS;
        //WHEN
        Throwable exception = assertThrows(OutOfReportingTimeException.class, () -> {
            underTest.calculateDueDate(localDateTime, turnaroundTime);
        });
        //THEN
        assertEquals(INVALID_SUBMIT_DATE, exception.getMessage());
    }

    @Test
    public void calculateDueDateShouldThrowOutOfReportingTimeExceptionWhenSubmitDateIsNull() {
        //GIVEN
        int turnaroundTime = EIGHT_HOURS;
        //WHEN
        Throwable exception = assertThrows(OutOfReportingTimeException.class, () -> {
            underTest.calculateDueDate(null, turnaroundTime);
        });
        //THEN
        assertEquals(INVALID_SUBMIT_DATE, exception.getMessage());
    }

    @Test
    public void calculateDueDateShouldThrowOutOfReportingTimeExceptionWhenTurnaroundTimeIsNotPositive() {
        //GIVEN
        LocalDateTime localDateTime = LocalDateTime.of(DATE_2018_07_02_MONDAY, TIME_14_12);
        int turnaroundTime = 0;
        //WHEN
        Throwable exception = assertThrows(OutOfReportingTimeException.class, () -> {
            underTest.calculateDueDate(localDateTime, turnaroundTime);
        });
        //THEN
        assertEquals(INVALID_SUBMIT_DATE, exception.getMessage());
    }
}